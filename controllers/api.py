import datetime
import base64
import json
import calendar
import numpy as np

#@auth.requires_login()
def index():
    if auth.user_id:
        user = db(db.auth_user.id==auth.user_id).select().first()
        if not user.api_key or request.vars.new_key:
            api_key=''.join(random.choice("0123456789abcdef") for x in range(32))
            db(db.auth_user.id==auth.user_id).update(api_key=api_key)
            user.api_key=api_key
        if not auth.user.api_key_b64 or request.vars.new_key:
            api_key_b64 ="Basic " + base64.b64encode(user.api_key + ":api_token")
            db(db.auth_user.id==auth.user_id).update(api_key_b64=api_key_b64)
            user.api_key_b64=api_key_b64
        if user.api_requests is None or user.api_last_request is None or (user.api_last_request + API_PERIOD < request.now):
            user.api_requests = 0
            db(db.auth_user.id==auth.user_id).update(api_requests=0)
            user.api_last_request = request.now
            db(db.auth_user.id==auth.user_id).update(api_last_request=request.now)
        else:
            pass#auth.user.api_requests += 1
    else:
        user = None
    return dict(user=user)

def v1():
    if not (request.is_https or request.env.remote_addr == "127.0.0.1"):
        raise HTTP(400,'URL is not sent through HTTPS. Please replace HTTP with HTTPS in the URL.')
    if not request.env.HTTP_AUTHORIZATION:
        raise HTTP(401,'An API key is required to access the API. Authentication is via HTTP Basic Auth, using the API key as the username and "api_token" as the password. Logging in on your web browser does not log you in to the API. Full directions on getting an API key and using it to log in are at: https://hypatiacatalog.com/api')
    user = db(db.auth_user.api_key_b64 == request.env.HTTP_AUTHORIZATION).select().first()
    if not user:
        raise HTTP(401,'Invalid API key. Make sure you have correctly applied the API key using HTTP Basic Auth. Full directions are at: https://hypatiacatalog.com/api')
    if user.api_requests >= API_LIMIT:
        raise HTTP(429,'You have exceeded your quota for the Hypatia Catalog API. Please try again later. You may see your quota at: https://hypatiacatalog.com/api')
    if user.api_last_request + API_PERIOD < request.now:
        db(db.auth_user.id==user.id).update(api_requests=1,api_last_request=request.now)
        response.headers["X-Rate-Limit-Limit"] = "1"
        response.headers["X-Rate-Limit-Remaining"] = str(API_LIMIT-1)
        response.headers["X-Rate-Limit-Reset"] = API_PERIOD.seconds
    else:
        db(db.auth_user.id==user.id).update(api_requests=user.api_requests+1)
        response.headers["X-Rate-Limit-Limit"] = str(user.api_requests+1)
        response.headers["X-Rate-Limit-Remaining"] = str(API_LIMIT-(user.api_requests+1))
        response.headers["X-Rate-Limit-Reset"] = API_PERIOD.seconds - (request.now-user.api_last_request).seconds
    verb = request.env.REQUEST_METHOD
    # all verbs in this case will be GET
    if verb != "GET":
        raise HTTP(400,"The Hypatia API only has GET commands. You submitted a %s command." % verb)
    noun = request.args[0]
    result = []
    if noun == "solarnorm":
        solarnorms = db(db.t_solarnorm.id>0).select()
        for solarnorm in solarnorms:
            result.append({
                "identifier":solarnorm.f_identifier,
                "author":solarnorm.f_author,
                "year":solarnorm.f_year,
                "version":solarnorm.f_version,
                "notes":solarnorm.f_notes
            })
    if noun == "element":
        elements = db(db.t_element.id>0).select()
        for element in elements:
            result.append(element.f_name)
    if noun == "catalog":
        catalogs = db(db.t_catalogue.id>0).select()
        for catalog in catalogs:
            result.append({
                "id":catalog.id,
                "author":catalog.f_author,
                "year":catalog.f_year,
                "version":catalog.f_version,
                "li":catalog.f_li=="True",
                "override_name":catalog.f_display_name
            })
    if noun == "star":
        starlist = []
        if request.vars.hip:
            if isinstance(request.vars.hip, list):
                for hip in request.vars.hip:
                    starlist.append({"hip":hip})
            else:
                starlist.append({"hip":request.vars.hip})
        if request.vars.hd:
            if isinstance(request.vars.hd, list):
                for hd in request.vars.hd:
                    starlist.append({"hd":hd})
            else:
                starlist.append({"hd":request.vars.hd})
        if request.vars.bd:
            if isinstance(request.vars.bd, list):
                for bd in request.vars.bd:
                    starlist.append({"bd":bd})
            else:
                starlist.append({"bd":request.vars.bd})
        if request.vars.twomass:
            if isinstance(request.vars.twomass, list):
                for twomass in request.vars.twomass:
                    starlist.append({"twomass":twomass})
            else:
                starlist.append({"twomass":request.vars.twomass})
        for star in starlist:
            if "hip" in star:
                stardata = db(db.t_star.f_hip==star['hip']).select().first()
                if stardata:
                    result.append(stardata)
                else:
                    result.append({
                        "hip":int(star['hip']),
                        "status":"not-found",
                        "hd":None,
                        "bd":None,
                        "spec":None,
                        "vmag":None,
                        "bv":None,
                        "dist":None,
                        "ra":None,
                        "dec":None,
                        "x":None,
                        "y":None,
                        "z":None,
                        "disk":None,
                        "u":None,
                        "v":None,
                        "w":None,
                        "teff":None,
                        "logg":None,
                        "2MASS":None,
                        "ra_proper_motion":None,
                        "dec_proper_motion":None,
                        "bmag":None,
                        "planets":None,
                    })
            if "hd" in star:
                stardata = db(db.t_star.f_hd==star['hd']).select().first()
                if stardata:
                    result.append(stardata)
                else:
                    result.append({
                        "hd":int(star['hd']),
                        "status":"not-found",
                        "hip":None,
                        "bd":None,
                        "spec":None,
                        "vmag":None,
                        "bv":None,
                        "dist":None,
                        "ra":None,
                        "dec":None,
                        "x":None,
                        "y":None,
                        "z":None,
                        "disk":None,
                        "u":None,
                        "v":None,
                        "w":None,
                        "teff":None,
                        "logg":None,
                        "2MASS":None,
                        "ra_proper_motion":None,
                        "dec_proper_motion":None,
                        "bmag":None,
                        "planets":None,
                    })
            if "bd" in star:
                stardata = db(db.t_star.f_bd==star['bd']).select().first()
                if stardata:
                    result.append(stardata)
                else:
                    result.append({
                        "bd":star['bd'],
                        "status":"not-found",
                        "hip":None,
                        "hd":None,
                        "spec":None,
                        "vmag":None,
                        "bv":None,
                        "dist":None,
                        "ra":None,
                        "dec":None,
                        "x":None,
                        "y":None,
                        "z":None,
                        "disk":None,
                        "u":None,
                        "v":None,
                        "w":None,
                        "teff":None,
                        "logg":None,
                        "2MASS":None,
                        "ra_proper_motion":None,
                        "dec_proper_motion":None,
                        "bmag":None,
                        "planets":None,
                    })
            if "twomass" in star:
                stardata = db(db.t_star.f_2mass==star['twomass']).select().first()
                if stardata:
                    result.append(stardata)
                else:
                    result.append({
                        "2MASS":star['twomass'],
                        "status":"not-found",
                        "hip":None,
                        "bd":None,
                        "spec":None,
                        "vmag":None,
                        "bv":None,
                        "dist":None,
                        "ra":None,
                        "dec":None,
                        "x":None,
                        "y":None,
                        "z":None,
                        "disk":None,
                        "u":None,
                        "v":None,
                        "w":None,
                        "teff":None,
                        "logg":None,
                        "hd":None,
                        "ra_proper_motion":None,
                        "dec_proper_motion":None,
                        "bmag":None,
                        "planets":None,
                    })
        for i in range(len(result)):
            if not isinstance(result[i],dict):
                stardata = result[i]
                planetdata = db(db.t_planet.f_star==stardata.id).select()
                planets = []
                for planet in planetdata:
                    planets.append({
                        "name":planet.f_name,
                        "m_p":planet.f_m_p,
                        "m_p_err":planet.f_m_p_err,
                        "p":planet.f_p,
                        "p_err":planet.f_p_err,
                        "e":planet.f_e,
                        "e_err":planet.f_e_err,
                        "a":planet.f_a,
                        "a_err":planet.f_a_err,
                    })
                result[i] = {
                        "hip":stardata.f_hip,
                        "hd":stardata.f_hd,
                        "bd":stardata.f_bd,
                        "spec":stardata.f_spec,
                        "vmag":stardata.f_vmag,
                        "bv":stardata.f_bv,
                        "dist":stardata.f_dist,
                        "ra":stardata.f_ra,
                        "dec":stardata.f_dec,
                        "x":stardata.f_x,
                        "y":stardata.f_y,
                        "z":stardata.f_z,
                        "disk":stardata.f_disk,
                        "u":stardata.f_u,
                        "v":stardata.f_v,
                        "w":stardata.f_w,
                        "teff":stardata.f_teff,
                        "logg":stardata.f_logg,
                        "2MASS":stardata.f_2mass,
                        "ra_proper_motion":stardata.f_ra_proper_motion,
                        "dec_proper_motion":stardata.f_dec_proper_motion,
                        "bmag":stardata.f_bmag,
                        "planets":planets,
                        "status":"found"
                    }
    if noun == "composition":
        hips = request.vars.hip
        elements = request.vars.element
        solarnorms = request.vars.solarnorm
        hip_ids = []
        element_ids = []
        solarnorm_ids = []

        if type(hips) != list:
            hips = [hips]
        for i in range(len(hips)):
            hip_ids.append(int(hips[i]))

        if type(elements) != list:
            elements = [elements]
        for i in range(len(elements)):
            if elements[i][-1] != "H":
                elements[i] = elements[i] + "H"
            element_ids.append(db(db.t_element.f_name.like(elements[i],case_sensitive=False)).select().first().id)

        if solarnorms == None:
            solarnorms = ["lod09"] * len(hips)
        elif type(solarnorms) != list:
            solarnorms = [solarnorms]
        for i in range(len(solarnorms)):
            solarnorm_ids.append(db(db.t_solarnorm.f_identifier.like(solarnorms[i],case_sensitive=False)).select().first().id)

        for hip, element, solarnorm, element_name, solarnorm_name in zip(hip_ids, element_ids, solarnorm_ids, elements, solarnorms):
            pairing = []
            values = []
            compositions = pd.read_hdf(os.path.join(HYP_DATA_DIR,"compositions.%s.h5" % solarnorm),"compositions")
            info = compositions[compositions.star_hip == hip][compositions.element == element]
            for value in zip(info.value,info.catalogue):
                catalog = db.t_catalogue[value[1]]
                pairing.append({"value": value[0],"catalog":{
                    "id":catalog.id,
                    "author":catalog.f_author,
                    "year":catalog.f_year,
                    "version":catalog.f_version,
                    "li":catalog.f_li=="True",
                    "override_name":catalog.f_display_name
                }})
                values.append(value[0])
            median = sorted(values)
            if len(median)%2==0:
                median = median[(len(median)/2)-1:(len(median)/2)+1]
            else:
                median = median[len(median)/2]
            median_pairings = [pair for pair in pairing if pair['value'] == median]
            result.append({"hip":hip,"element":element_name,"solarnorm":solarnorm_name,"all_values":pairing,"median":median_pairings,"mean":sum(values)/len(pairing)})
    if noun == "data":
        inputs = Storage()
        for key in request.vars.keys():
            inputs[key] = request.vars[key]
        inputs.filter1_1 = inputs.filter1_1 or "none"
        inputs.filter1_2 = inputs.filter1_2 or "H"
        inputs.filter2_1 = inputs.filter2_1 or "none"
        inputs.filter2_2 = inputs.filter2_2 or "H"
        inputs.filter3_1 = inputs.filter3_1 or "none"
        inputs.filter3_2 = inputs.filter3_2 or "H"
        inputs.xaxis1 = inputs.xaxis1 or "Fe"
        inputs.xaxis2 = inputs.xaxis2 or "H"
        inputs.yaxis1 = inputs.yaxis1 or "Si"
        inputs.yaxis2 = inputs.yaxis2 or "H"
        inputs.zaxis1 = inputs.zaxis1 or "none"
        inputs.zaxis2 = inputs.zaxis2 or "H"
        inputs.cat_action = inputs.cat_action or "exclude" 
        inputs.statistic = inputs.statistic or "median" 
        inputs.tablecols = inputs.tablecols or "Fe,C,O,Mg,Si,S,Ca,Ti"
        inputs.tablesource = inputs.tablesource or "graph"
        if type(inputs.tablecols) == str:
            inputs.tablecols = inputs.tablecols.split(",")
        if request.vars.graph_submit and not request.vars.xaxislog:
            inputs.xaxislog = False
        if request.vars.graph_submit and not request.vars.yaxislog:
            inputs.yaxislog = False
        if request.vars.graph_submit and not request.vars.zaxislog:
            inputs.zaxislog = False
        if request.vars.graph_submit and not request.vars.xaxisinv:
            inputs.xaxisinv = False
        if request.vars.graph_submit and not request.vars.yaxisinv:
            inputs.yaxisinv = False
        if request.vars.graph_submit and not request.vars.zaxisinv:
            inputs.zaxisinv = False
        if request.vars.graph_submit and not request.vars.filter1_inv:
            inputs.filter1_inv = False
        if request.vars.graph_submit and not request.vars.filter2_inv:
            inputs.filter2_inv = False
        if request.vars.graph_submit and not request.vars.filter3_inv:
            inputs.filter3_inv = False
        if request.vars.graph_submit and not request.vars.normalize:
            inputs.normalize = False
        if request.vars.graph_submit and not request.vars.catalogs:
            inputs.catalogs = []
        if request.vars.graph_submit and type(request.vars.catalogs) == str:
            inputs.catalogs = [request.vars.catalogs]
        if inputs.catalogs:
            inputs.catalogs = [intable(i,invalid=-1) for i in inputs.catalogs]
        if request.vars.mode != "hist":
            inputs.mode = "scatter"
        
        solarnorm = db(db.t_solarnorm.f_identifier == request.vars.solarnorm).select().first()
        try:
            inputs.solarnorm = int(solarnorm.id)
        except:
            solarnorm = db(db.t_solarnorm.f_author.contains("Lodders")).select().first()
            try:
                inputs.solarnorm = int(solarnorm.id)
            except:
                solarnorm = db(db.t_solarnorm.id>0).select().first()
                inputs.solarnorm = int(solarnorm.id)
        
        catalogs = db(db.t_catalogue.id>0).select(orderby=db.t_catalogue.f_author.lower()|db.t_catalogue.f_year|db.t_catalogue.f_version)
        
        #define variables
        elements = {}
        stellarProps = {}
        planetProps = {}
        filters = {}
        planetFilters = {}
        inputs.hipcode = None
        
        #which axes are needed?
        #histogram: X only
        #scatter: X, Y
        #scatter with color: X, Y, Z
        axes = ['xaxis']
        if inputs.mode == "scatter":
            axes.append('yaxis')
            if inputs.zaxis1 != "none":
                axes.append('zaxis')
        
        #filter the data
        if inputs.filter1_1 != "none" and (floatable(inputs.filter1_3) != None or floatable(inputs.filter1_4) != None):
            axes.append('filter1_')
            if inputs.filter1_1 in ["p","m_p","e","a"]:
                planetFilters['filter1_'] = (floatable(inputs.filter1_3),floatable(inputs.filter1_4),inputs.filter1_inv)
            else:
                filters['filter1_'] = (floatable(inputs.filter1_3),floatable(inputs.filter1_4),inputs.filter1_inv)
        if inputs.filter2_1 != "none" and (floatable(inputs.filter2_3) != None or floatable(inputs.filter2_4) != None):
            axes.append('filter2_')
            if inputs.filter2_1 in ["p","m_p","e","a"]:
                planetFilters['filter2_'] = (floatable(inputs.filter2_3),floatable(inputs.filter2_4),inputs.filter2_inv)
            else:
                filters['filter2_'] = (floatable(inputs.filter2_3),floatable(inputs.filter2_4),inputs.filter2_inv)
        if inputs.filter3_1 != "none" and (floatable(inputs.filter3_3) != None or floatable(inputs.filter3_4) != None):
            axes.append('filter3_')
            if inputs.filter3_1 in ["p","m_p","e","a"]:
                planetFilters['filter3_'] = (floatable(inputs.filter3_3),floatable(inputs.filter3_4),inputs.filter3_inv)
            else:
                filters['filter3_'] = (floatable(inputs.filter3_3),floatable(inputs.filter3_4),inputs.filter3_inv)
            
        #handle axes
        for item in axes:
            elements[item+"1"] = db(db.t_element.f_name==inputs[item+"1"]+"H").select().first()
            if elements[item+"1"]:
                elements[item+"1"] = elements[item+"1"].id
                if inputs[item+"2"] != 'H':
                    elements[item+"2"] = db(db.t_element.f_name==inputs[item+"2"]+"H").select().first()
                    if elements[item+"2"]:
                        elements[item+"2"] = elements[item+"2"].id
            elif inputs.get(item+"1") in ["p","m_p","e","a"]:
                planetProps[item] = inputs.get(item+"1")
            else:
                stellarProps[item] = inputs.get(item+"1")
        
        #get compositions
        compositions = pd.read_hdf(os.path.join(HYP_DATA_DIR,"compositions.%s.h5" % inputs.solarnorm),"compositions")
        
        #include/exclude catalogs
        if inputs.catalogs and inputs.cat_action == "exclude":
            compositions = compositions[~compositions.catalogue.isin(inputs.catalogs)]
        elif inputs.catalogs:
            compositions = compositions[compositions.catalogue.isin(inputs.catalogs)]
        
        #include/exclude stars
        if inputs.star_list:
            #convert all hds to hips
            if inputs.star_source == "hd":
                hdData = inputs.star_list.split(",")
                hipData = []
                stars = db(db.t_star.id>0).select()
                for hd in hdData:
                    hdint = intable(hd,invalid=-1)
                    star = stars.find(lambda s: s.f_hd == hdint)
                    if star:
                        hipData.append(star.first().f_hip)
                    print repr(hipData)
            else:
                #build the list of hips
                hipData = [intable(hip,invalid=-1) for hip in inputs.star_list.split(",")]
            if inputs.star_action == "exclude":
                compositions = compositions[~compositions.star_hip.isin(hipData)]
            else:
                compositions = compositions[compositions.star_hip.isin(hipData)]
        
        
        #get all the hips
        stars = db(db.t_star.id>0).select(db.t_star.f_hip)
        hips = [star.f_hip for star in stars]
        
        #get planet/stellar properties if needed
        if (len(stellarProps) + len(planetProps) > 0) or inputs.mode=="hist":
            hashTable = shelve.open(os.path.join(HYP_DATA_DIR,"hashtable.shelf"))
        
        #get compositions relevant to scatter plot
        xy_data = compositions[compositions.element.isin(elements.values())]
        
        #add abundances
        myStars = Stars()
        for abundance in xy_data.itertuples():
            myStars.addAbundance(abundance.star_hip,abundance.element,abundance.value)
            
        #generate outputs
        outputs = {'hip':[]}
        for axis in axes:
            outputs[axis] = []
        value = {}
        for hip in hips:
            for item in axes:
                if item in planetProps: #planet parameter
                    value[item] = random.randint(9000,9999) #we'll fill this out later
                    continue
                if item in stellarProps: #stellar parameter
                    value[item] = hashTable['star-%s' % hip].get("f_"+stellarProps[item])
                    if value[item] == 9999:
                        value[item] = None
                    if value[item] == "thin":
                        value[item] = 0
                    if value[item] == "thick":
                        value[item] = 1
                    if value[item] == "N/A":
                        value[item] = None
                    if stellarProps[item] == "spec":
                        value[item] = spectype(value[item])
                    continue
                value[item] = myStars.getStatistic(hip,elements[item+"1"]) #element ratio
                if value[item] == None:
                    continue
                if item+"2" in elements: #denominator is not H
                    value2 = myStars.getStatistic(hip,elements[item+"2"])
                    if value2 == None:
                        value[item] = None
                        continue
                    value[item] -= value2
            #only plot if there is a value for each axis and it matches the filter
            if (all([value[axis] != None for axis in axes]) 
                    and all([check_filter(value[f],filters[f]) for f in filters])):
                for axis in axes:
                    outputs[axis].append(value[axis])
                outputs['hip'].append(hip)
        
        #if there are any planet parameters, then each data point should be
        #a planet as opposed to a star. Start the process again
        if len(planetProps) > 0:
            planet_outputs = {'hip':[]}
            for axis in axes:
                planet_outputs[axis] = []
            value = {}
            hiplist = []
            for i in range(len(outputs['hip'])):
                starid = hashTable['starid-%s' % outputs['hip'][i]][0]
                for char in "bcdefghijklmnopqrstuvwxyz":
                    if "planet-%s-%s" % (starid,char) not in hashTable:
                        break
                    for item in axes:
                        if item in planetProps: #planet parameter
                            value[item] = hashTable['planet-%s-%s' % (starid,char)].get("f_"+planetProps[item])
                            if value[item] == 999:
                                value[item] = None
                        else: #leftover stellar parameter or element ratio
                            value[item] = outputs[item][i]
                    #only plot if there is a value for each axis and it matches the filter
                    if (all([value[axis] for axis in axes])
                            and all([check_filter(value[f],planetFilters[f]) for f in planetFilters])):
                        for axis in axes:
                            planet_outputs[axis].append(value[axis])
                        planet_outputs['hip'].append(str(outputs['hip'][i])+char)
                        hiplist.append(outputs['hip'][i])
            outputs = planet_outputs
        else:
            hiplist = outputs['hip']
                    
        #build the plot
        TOOLS="crosshair,pan,wheel_zoom,zoom_in,zoom_out,box_zoom,undo,redo,reset,tap,save,box_select,poly_select,lasso_select,"     
        
        #build the labels
        labels = {}
        unique_labels = {}
        for axis in axes:
            if axis in stellarProps:
                labels[axis] = inputs.get(axis+"1")
            elif axis in planetProps:
                labels[axis] = inputs.get(axis+"1")
            else:
                labels[axis] = "[%s/%s]" % (inputs.get(axis+"1"),inputs.get(axis+"2"))
            if labels[axis] not in unique_labels.values():
                unique_labels[axis] = labels[axis]
        
        #set the axis type: log or linear
        x_axis_type=(inputs.xaxislog and "log" or "linear")
        y_axis_type=(inputs.yaxislog and "log" or "linear")
        
        #if there is no data then return a message
        if len(outputs['xaxis']) == 0:
            result = {"count":0}

        #histogram
        elif inputs.mode == "hist":
            #counts stars with planets
            with_planet = []
            for i in range(len(outputs['xaxis'])):
                getstarid = outputs['hip'][i]
                try:
                    getstarid = re.sub("[^0-9]", "", getstarid)
                except:
                    pass
                starid = hashTable['starid-%s' % getstarid][0]
                if ("planet-%s-b" % starid) in hashTable:
                    with_planet.append(outputs['xaxis'][i])
            #builds the histogram
            hist_all, edges = np.histogram(outputs['xaxis'],bins=20)
            hist_planet, edges = np.histogram(with_planet, bins=edges)
            #get maximum point on the histogram
            max_hist_all = float(max(hist_all))
            max_hist_planet = float(max(hist_planet))
            #normalize if necessary
            if inputs.normalize:
                hist_all = hist_all/max_hist_all
                hist_planet = hist_planet/max_hist_planet
                max_hist_all = 1
                max_hist_planet = 1
                labels['yaxis'] = "Relative Frequency"
                fill_alpha = 0.5
                line_alpha = 0.2
            else:
                labels['yaxis'] = 'Number of Stellar Systems'
                fill_alpha = 1
                line_alpha = 1
            result = {"all_hypatia":hist_all.tolist(),"exo_hosts":hist_planet.tolist(),"edges":edges.tolist(),"labels":labels,"count":len(outputs['xaxis'])}
        else: # Scatter
            for i in range(len(outputs['hip'])):
                point = {}
                for col in outputs:
                    point[col.replace("_","")] = outputs[col][i]
                result.append(point)
            for col in labels:
                if "_" in col:
                    labels[col.replace("_","")] = labels[col]
                    del(labels[col])
            result = {"values":result,"labels":labels,"count":len(outputs['xaxis'])}
        result['solarnorm'] = {
            "identifier":solarnorm.f_identifier,
            "author":solarnorm.f_author,
            "year":solarnorm.f_year,
            "version":solarnorm.f_version,
            "notes":solarnorm.f_notes
        }
    response.headers['Content-Type'] = 'application/json'
    return json.dumps(result)