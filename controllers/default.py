import os
import copy
import numpy as np
from bokeh.plotting import figure, show, output_file, ColumnDataSource
from bokeh.models import HoverTool, ColumnDataSource, ColorBar, LinearColorMapper, LogColorMapper, CustomJS
from bokeh.embed import components
from bokeh.palettes import Viridis3, Viridis256
from bokeh.io import export_png, export_svgs
import pandas as pd
import shelve
import uuid
import json
import random
import re

# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

#this is the front page
def index():
    counts = {}
    counts['stars'] = db(db.t_star.id>0).count()
    planets = db(db.t_planet.id>0).select(db.t_planet.f_star)
    stars_with_planets = {}
    for p in planets:
        stars_with_planets[p.f_star] = 1
    counts['stars_with_planets'] = len(stars_with_planets)
    counts['elements'] = db(~db.t_element.f_name.contains("blank")).count()
    counts['catalogs'] = db(db.t_catalogue.id>0).count()
    lodders = db(db.t_solarnorm.f_author.contains("Lodders")).select().first().id
    compositions = pd.read_hdf(os.path.join(HYP_DATA_DIR,"compositions.%s.h5" % lodders),"compositions")
    counts['abundances'] = len(compositions)
    return dict(counts=counts)
    
def help():
    return dict()
    
def about():
    return dict()
    
def credits():
    return dict()

def launch():
    for key in request.vars.keys():
        session[key] = request.vars[key]
    session.filter1_1 = session.filter1_1 or "none"
    session.filter1_2 = session.filter1_2 or "H"
    session.filter2_1 = session.filter2_1 or "none"
    session.filter2_2 = session.filter2_2 or "H"
    session.filter3_1 = session.filter3_1 or "none"
    session.filter3_2 = session.filter3_2 or "H"
    session.xaxis1 = session.xaxis1 or "Fe"
    session.xaxis2 = session.xaxis2 or "H"
    session.yaxis1 = session.yaxis1 or "Si"
    session.yaxis2 = session.yaxis2 or "H"
    session.zaxis1 = session.zaxis1 or "none"
    session.zaxis2 = session.zaxis2 or "H"
    session.cat_action = session.cat_action or "exclude" 
    session.statistic = session.statistic or "median" 
    session.tablecols = session.tablecols or "Fe,C,O,Mg,Si,S,Ca,Ti"
    session.tablesource = session.tablesource or "graph"
    if type(session.tablecols) == str:
        session.tablecols = session.tablecols.split(",")
    if request.vars.graph_submit and not request.vars.xaxislog:
        session.xaxislog = False
    if request.vars.graph_submit and not request.vars.yaxislog:
        session.yaxislog = False
    if request.vars.graph_submit and not request.vars.zaxislog:
        session.zaxislog = False
    if request.vars.graph_submit and not request.vars.xaxisinv:
        session.xaxisinv = False
    if request.vars.graph_submit and not request.vars.yaxisinv:
        session.yaxisinv = False
    if request.vars.graph_submit and not request.vars.zaxisinv:
        session.zaxisinv = False
    if request.vars.graph_submit and not request.vars.filter1_inv:
        session.filter1_inv = False
    if request.vars.graph_submit and not request.vars.filter2_inv:
        session.filter2_inv = False
    if request.vars.graph_submit and not request.vars.filter3_inv:
        session.filter3_inv = False
    if request.vars.graph_submit and not request.vars.normalize:
        session.normalize = False
    if request.vars.graph_submit and not request.vars.catalogs:
        session.catalogs = []
    if request.vars.graph_submit and type(request.vars.catalogs) == str:
        session.catalogs = [request.vars.catalogs]
    if session.catalogs:
        session.catalogs = [intable(i,invalid=-1) for i in session.catalogs]
    session.mode = request.vars.mode or "scatter"
    
    solarnorms = db(db.t_solarnorm.id>0).select(orderby=~db.t_solarnorm.f_year|~db.t_solarnorm.f_version|~db.t_solarnorm.f_author.lower())
    lodders = db(db.t_solarnorm.f_author.contains("Lodders")).select().first().id
    try:
        session.solarnorm = int(session.solarnorm) or lodders
    except:
        session.solarnorm = solarnorms[0].id
    
    catalogs = db(db.t_catalogue.id>0).select(orderby=db.t_catalogue.f_author.lower()|db.t_catalogue.f_year|db.t_catalogue.f_version)
    
    return dict(catalogs=catalogs, solarnorms=solarnorms)

def dropdown():
    return dict()

def error():
    return dict()
    
def graph():
    for key in request.vars.keys():
        session[key] = request.vars[key]
    session.filter1_1 = session.filter1_1 or "none"
    session.filter1_2 = session.filter1_2 or "H"
    session.filter2_1 = session.filter2_1 or "none"
    session.filter2_2 = session.filter2_2 or "H"
    session.filter3_1 = session.filter3_1 or "none"
    session.filter3_2 = session.filter3_2 or "H"
    session.xaxis1 = session.xaxis1 or "Fe"
    session.xaxis2 = session.xaxis2 or "H"
    session.yaxis1 = session.yaxis1 or "Si"
    session.yaxis2 = session.yaxis2 or "H"
    session.zaxis1 = session.zaxis1 or "none"
    session.zaxis2 = session.zaxis2 or "H"
    session.cat_action = session.cat_action or "exclude" 
    session.statistic = session.statistic or "median" 
    session.tablecols = session.tablecols or "Fe,C,O,Mg,Si,S,Ca,Ti"
    session.tablesource = session.tablesource or "graph"
    if type(session.tablecols) == str:
        session.tablecols = session.tablecols.split(",")
    if request.vars.graph_submit and not request.vars.xaxislog:
        session.xaxislog = False
    if request.vars.graph_submit and not request.vars.yaxislog:
        session.yaxislog = False
    if request.vars.graph_submit and not request.vars.zaxislog:
        session.zaxislog = False
    if request.vars.graph_submit and not request.vars.xaxisinv:
        session.xaxisinv = False
    if request.vars.graph_submit and not request.vars.yaxisinv:
        session.yaxisinv = False
    if request.vars.graph_submit and not request.vars.zaxisinv:
        session.zaxisinv = False
    if request.vars.graph_submit and not request.vars.filter1_inv:
        session.filter1_inv = False
    if request.vars.graph_submit and not request.vars.filter2_inv:
        session.filter2_inv = False
    if request.vars.graph_submit and not request.vars.filter3_inv:
        session.filter3_inv = False
    if request.vars.graph_submit and not request.vars.normalize:
        session.normalize = False
    if request.vars.graph_submit and not request.vars.catalogs:
        session.catalogs = []
    if request.vars.graph_submit and type(request.vars.catalogs) == str:
        session.catalogs = [request.vars.catalogs]
    if session.catalogs:
        session.catalogs = [intable(i,invalid=-1) for i in session.catalogs]
    if type(session.mode) == list:
        session.mode = session.mode[0]

    #define variables
    elements = {}
    stellarProps = {}
    planetProps = {}
    filters = {}
    planetFilters = {}
    session.hipcode = None
    
    #which axes are needed?
    #histogram: X only
    #scatter: X, Y
    #scatter with color: X, Y, Z
    axes = ['xaxis']
    if session.mode == "scatter":
        axes.append('yaxis')
        if session.zaxis1 != "none":
            axes.append('zaxis')
    
    #filter the data
    if session.filter1_1 != "none" and (floatable(session.filter1_3) != None or floatable(session.filter1_4) != None):
        axes.append('filter1_')
        if session.filter1_1 in ["p","m_p","e","a"]:
            planetFilters['filter1_'] = (floatable(session.filter1_3),floatable(session.filter1_4),session.filter1_inv)
        else:
            filters['filter1_'] = (floatable(session.filter1_3),floatable(session.filter1_4),session.filter1_inv)
    if session.filter2_1 != "none" and (floatable(session.filter2_3) != None or floatable(session.filter2_4) != None):
        axes.append('filter2_')
        if session.filter2_1 in ["p","m_p","e","a"]:
            planetFilters['filter2_'] = (floatable(session.filter2_3),floatable(session.filter2_4),session.filter2_inv)
        else:
            filters['filter2_'] = (floatable(session.filter2_3),floatable(session.filter2_4),session.filter2_inv)
    if session.filter3_1 != "none" and (floatable(session.filter3_3) != None or floatable(session.filter3_4) != None):
        axes.append('filter3_')
        if session.filter3_1 in ["p","m_p","e","a"]:
            planetFilters['filter3_'] = (floatable(session.filter3_3),floatable(session.filter3_4),session.filter3_inv)
        else:
            filters['filter3_'] = (floatable(session.filter3_3),floatable(session.filter3_4),session.filter3_inv)
        
    #handle axes
    for item in axes:
        elements[item+"1"] = db(db.t_element.f_name==session[item+"1"]+"H").select().first()
        if elements[item+"1"]:
            elements[item+"1"] = elements[item+"1"].id
            if session[item+"2"] != 'H':
                elements[item+"2"] = db(db.t_element.f_name==session[item+"2"]+"H").select().first()
                if elements[item+"2"]:
                    elements[item+"2"] = elements[item+"2"].id
        elif session.get(item+"1") in ["p","m_p","e","a"]:
            planetProps[item] = session.get(item+"1")
        else:
            stellarProps[item] = session.get(item+"1")
    solarnorm = intable(session.solarnorm)
    
    #get compositions
    compositions = pd.read_hdf(os.path.join(HYP_DATA_DIR,"compositions.%s.h5" % solarnorm),"compositions")
    
    #include/exclude catalogs
    if session.catalogs and session.cat_action == "exclude":
        compositions = compositions[~compositions.catalogue.isin(session.catalogs)]
    elif session.catalogs:
        compositions = compositions[compositions.catalogue.isin(session.catalogs)]
    
    #include/exclude stars
    if session.star_list:
        #convert all hds to hips
        if session.star_source == "hd":
            hdData = session.star_list.split(",")
            hipData = []
            stars = db(db.t_star.id>0).select()
            for hd in hdData:
                hdint = intable(hd,invalid=-1)
                star = stars.find(lambda s: s.f_hd == hdint)
                if star:
                    hipData.append(star.first().f_hip)
                print repr(hipData)
        else:
            #build the list of hips
            hipData = [intable(hip,invalid=-1) for hip in session.star_list.split(",")]
        if session.star_action == "exclude":
            compositions = compositions[~compositions.star_hip.isin(hipData)]
        else:
            compositions = compositions[compositions.star_hip.isin(hipData)]
    
    
    #get all the hips
    stars = db(db.t_star.id>0).select(db.t_star.f_hip)
    hips = [star.f_hip for star in stars]
    
    #get planet/stellar properties if needed
    if (len(stellarProps) + len(planetProps) > 0) or session.mode=="hist":
        hashTable = shelve.open(os.path.join(HYP_DATA_DIR,"hashtable.shelf"))
    
    #get compositions relevant to scatter plot
    xy_data = compositions[compositions.element.isin(elements.values())]
    
    #add abundances
    myStars = Stars()
    for abundance in xy_data.itertuples():
        myStars.addAbundance(abundance.star_hip,abundance.element,abundance.value)
        
    #generate outputs
    outputs = {'hip':[]}
    for axis in axes:
        outputs[axis] = []
    value = {}
    for hip in hips:
        for item in axes:
            if item in planetProps: #planet parameter
                value[item] = random.randint(9000,9999) #we'll fill this out later
                continue
            if item in stellarProps: #stellar parameter
                value[item] = hashTable['star-%s' % hip].get("f_"+stellarProps[item])
                if value[item] == 9999:
                    value[item] = None
                if value[item] == "thin":
                    value[item] = 0
                if value[item] == "thick":
                    value[item] = 1
                if value[item] == "N/A":
                    value[item] = None
                if stellarProps[item] == "spec":
                    value[item] = spectype(value[item])
                continue
            value[item] = myStars.getStatistic(hip,elements[item+"1"]) #element ratio
            if value[item] == None:
                continue
            if item+"2" in elements: #denominator is not H
                value2 = myStars.getStatistic(hip,elements[item+"2"])
                if value2 == None:
                    value[item] = None
                    continue
                value[item] -= value2
        #only plot if there is a value for each axis and it matches the filter
        if (all([value[axis] != None for axis in axes]) 
                and all([check_filter(value[f],filters[f]) for f in filters])):
            for axis in axes:
                outputs[axis].append(value[axis])
            outputs['hip'].append(hip)
    
    #if there are any planet parameters, then each data point should be
    #a planet as opposed to a star. Start the process again
    if len(planetProps) > 0:
        planet_outputs = {'hip':[]}
        for axis in axes:
            planet_outputs[axis] = []
        value = {}
        hiplist = []
        for i in range(len(outputs['hip'])):
            starid = hashTable['starid-%s' % outputs['hip'][i]][0]
            for char in "bcdefghijklmnopqrstuvwxyz":
                if "planet-%s-%s" % (starid,char) not in hashTable:
                    break
                for item in axes:
                    if item in planetProps: #planet parameter
                        value[item] = hashTable['planet-%s-%s' % (starid,char)].get("f_"+planetProps[item])
                        if value[item] == 999:
                            value[item] = None
                    else: #leftover stellar parameter or element ratio
                        value[item] = outputs[item][i]
                #only plot if there is a value for each axis and it matches the filter
                if (all([value[axis] for axis in axes])
                        and all([check_filter(value[f],planetFilters[f]) for f in planetFilters])):
                    for axis in axes:
                        planet_outputs[axis].append(value[axis])
                    planet_outputs['hip'].append(str(outputs['hip'][i])+char)
                    hiplist.append(outputs['hip'][i])
        outputs = planet_outputs
    else:
        hiplist = outputs['hip']
                   
    #build the plot
    TOOLS="crosshair,pan,wheel_zoom,zoom_in,zoom_out,box_zoom,undo,redo,reset,tap,save,box_select,poly_select,lasso_select,"     
    
    #build the labels
    labels = {}
    unique_labels = {}
    for axis in axes:
        if axis in stellarProps:
            labels[axis] = session.get(axis+"1")
        elif axis in planetProps:
            labels[axis] = session.get(axis+"1")
        else:
            labels[axis] = "[%s/%s]" % (session.get(axis+"1"),session.get(axis+"2"))
        if labels[axis] not in unique_labels.values():
            unique_labels[axis] = labels[axis]
    
    #set the axis type: log or linear
    x_axis_type=(session.xaxislog and "log" or "linear")
    y_axis_type=(session.yaxislog and "log" or "linear")
    
    #if there is no data then return a message
    if len(outputs['xaxis']) == 0:
        return "No data points to display"
        
    #histogram
    if session.mode == "hist":
        #counts stars with planets
        with_planet = []
        for i in range(len(outputs['xaxis'])):
            getstarid = outputs['hip'][i]
            try:
                getstarid = re.sub("[^0-9]", "", getstarid)
            except:
                pass
            starid = hashTable['starid-%s' % getstarid][0]
            if ("planet-%s-b" % starid) in hashTable:
                with_planet.append(outputs['xaxis'][i])
        #builds the histogram
        hist_all, edges = np.histogram(outputs['xaxis'],bins=20)
        hist_planet, edges = np.histogram(with_planet, bins=edges)
        #get maximum point on the histogram
        max_hist_all = float(max(hist_all))
        max_hist_planet = float(max(hist_planet))
        #normalize if necessary
        if session.normalize:
            hist_all = hist_all/max_hist_all
            hist_planet = hist_planet/max_hist_planet
            max_hist_all = 1
            max_hist_planet = 1
            labels['yaxis'] = "Relative Frequency"
            fill_alpha = 0.5
            line_alpha = 0.2
        else:
            labels['yaxis'] = 'Number of Stellar Systems'
            fill_alpha = 1
            line_alpha = 1
        #set the bounds of the plot
        x_diff = max(outputs['xaxis']) - min(outputs['xaxis'])
        x_range = [min(outputs['xaxis']),max(outputs['xaxis'])]
        #invert the plot if necessary
        if session.xaxisinv:
            x_range = x_range[::-1]
        #build the plot object
        p = figure(tools=[TOOLS], width=750, height=625, 
            x_range=x_range, 
            y_range=[0,max_hist_all*1.20])
        p.quad(top=hist_all, bottom=0, left=edges[:-1], right=edges[1:],
            fill_color="maroon", line_color="black", fill_alpha=fill_alpha, line_alpha=line_alpha, legend="All Hypatia")
        p.quad(top=hist_planet, bottom=0, left=edges[:-1], right=edges[1:],
            fill_color="orange", line_color="black", fill_alpha=fill_alpha, line_alpha=line_alpha, legend="Exo-Hosts")
        
    else: #Scatter
        #handle tooltips
        source = ColumnDataSource(outputs)
        tooltips = "<b>HIP @hip</b><br/><div style='max-width:300px'>" + ", ".join([labels[axis]+" = @"+axis+"{0.00}" for axis in unique_labels]) + "</div>"
        hover = HoverTool(tooltips=tooltips)
        #build the bounds
        x_diff = max(outputs['xaxis']) - min(outputs['xaxis'])
        y_diff = max(outputs['yaxis']) - min(outputs['yaxis'])
        x_range = [min(outputs['xaxis'])-(0.10*x_diff),max(outputs['xaxis'])+(0.10*x_diff)]
        y_range = [min(outputs['yaxis'])-(0.10*y_diff),max(outputs['yaxis'])+(0.10*y_diff)]
        #invert the axis if necessary
        if session.xaxisinv:
            x_range = x_range[::-1]
        if session.yaxisinv:
            y_range = y_range[::-1]
        #build the figure
        p = figure(tools=[TOOLS,hover], width=750, height=625, 
            x_range=x_range, 
            y_range=y_range,
            x_axis_type=x_axis_type,y_axis_type=y_axis_type)
            
        #color if needed
        if 'zaxis' in axes:
            palette = Viridis256
            #invert the z-axis if necessary
            if session.zaxisinv:
                palette = palette[::-1]
            #set up a log scale if necessary
            if session.zaxislog:
                mapper = LogColorMapper(palette=palette, low=min(outputs['zaxis']), high=max(outputs['zaxis']))
            else:
                mapper = LinearColorMapper(palette=palette, low=min(outputs['zaxis']), high=max(outputs['zaxis']))
            #bulid the scatter plot
            p.scatter('xaxis', 'yaxis', fill_color={'field': 'zaxis', 'transform': mapper}, 
                line_color={'field': 'zaxis', 'transform': mapper}, 
                fill_alpha=0.3, line_alpha=0.8, source=source, size=8)
            color_bar = ColorBar(color_mapper=mapper, height=100, title=labels['zaxis'], border_line_width=1, border_line_color='#cccccc', label_standoff=7)
            p.add_layout(color_bar)
        else:
            #build the scatter plot
            p.scatter('xaxis', 'yaxis', fill_color='#4E11B7', line_color='#4E11B7', fill_alpha=0.3, line_alpha=0.6, source=source, size=8)
        
        #callback
        source.callback = CustomJS(code="""
            var inds = cb_obj.selected['1d'].indices;
            var d1 = cb_obj.data;
            var result = [];
            for (i = 0; i < inds.length; i++) {
                result.push(d1['hip'][inds[i]]);
            }
            $("#star_list").val(result.join(","));
            $("select[name='star_action']").val("only");
            $("select[name='star_source']").val("hip")
        """)
        
    #miscellaneous settings
    p.xaxis.axis_label = labels['xaxis']
    p.yaxis.axis_label = labels['yaxis']
    p.xaxis.axis_label_text_font_size = "12pt"
    p.xaxis.axis_label_text_font_style = "normal"
    p.xaxis.major_label_text_font_size = "12pt"
    p.xaxis.major_label_text_font_style = "normal"
    p.yaxis.axis_label_text_font_size = "12pt"
    p.yaxis.axis_label_text_font_style = "normal"
    p.yaxis.major_label_text_font_size = "12pt"
    p.yaxis.major_label_text_font_style = "normal"
    
    #save all the hips to the hips directory
    hipcode = str(uuid.uuid4())
    with open(os.path.join(HYP_DATA_DIR,"hips",hipcode),'w') as f:
        f.write(json.dumps(hiplist))
    session.hipcode = hipcode

    #generate PNG, SVG
    if request.extension == "png":
        return export_png(p)
    elif request.extension == "svg":
        p.output_backend = "svg"
        return export_svgs(p)
        
    #generate HTML
    script, div = components(p)
    
    #send back to the browser
    return dict(script=script,div=div)
    
def table():
    if not session.tablecols:
        return "Unable to retrieve table data. Please try again."
    if request.vars.tablecols:
        session.tablecols = request.vars.tablecols.split(",")
    """#convert all hds to hips
    if session.tablesource=="list" and session.tablelist and session.tableidentifier == "hd":
        hdData = session.tablelist.split(",")
        hipData = []
        stars = db(db.t_star.id>0).select()
        for hd in hdData:
            hdint = intable(hd,invalid=-1)
            star = stars.find(lambda s: s.f_hd == hdint)
            if star:
                hipData.append(star.first().f_hip)
            print repr(hipData)
    elif session.tablesource=="list" and session.tablelist:
        #build the list of hips
        hipData = [intable(hip,invalid=-1) for hip in session.tablelist.split(",")]
    elif session.hipcode:
        #load the list of hips
        with open(os.path.join(HYP_DATA_DIR,"hips",session.hipcode)) as f:
            hipData = json.loads(f.read())
    else:
        return "Could not retrieve stars from the graph above. Please try again."""
    with open(os.path.join(HYP_DATA_DIR,"hips",session.hipcode)) as f:
        hipData = set(json.loads(f.read()))
        
    #which stellar parameters to use?
    if "stellar" in session.tablecols:
        starCols2 = ["f_ra","f_dec","f_x","f_y","f_z","f_dist","f_disk","f_spec","f_vmag","f_bv","f_u","f_v","f_w","f_teff","f_logg","f_mass"]
    else:
        starCols2 = []
    starCols1 = ["f_hip","f_hd","f_2mass"]
    
    #which elements to use?
    elements_to_use = []
    for item in ['Fe','C','O','Mg','Si','Ca','Ti','Li','N','F','Na','Al','P','Cl','K','Sc','V','Cr','Mn','Co','Ni','Cu','Zn','Rb','Sr','Y','Zr','Nb','Ba','La','Ag','Eu','Gd','Tb','Tm','Ir','Th','Mo','Ru','Pd','Sn','Ce','Pr','Nd','Sm','Dy','Er','Yb','Hf','Pb']:
        if item in session.tablecols:
            elements_to_use.append(item)
        if item+"II" in session.tablecols:
            elements_to_use.append(item+"II")
    for item in session.tablecols:
        if item[0].isupper() and item not in elements_to_use:
            elements_to_use.append(item)
    
    #are we going to use the spread?
    if "spread" in session.tablecols:
        elementCols = []
        for item in elements_to_use:
            elementCols.append(item)
            elementCols.append(item+"_err")
    else:
        elementCols = elements_to_use
        
    #planet parameters
    if "planet" in session.tablecols:
        if "spread" in session.tablecols:
            planetCols = ["f_name","f_p","f_p_err","f_m_p","f_m_p_err","f_e","f_e_err","f_a","f_a_err"]
        else:
            planetCols = ["f_name","f_p","f_m_p","f_e","f_a"]
    else:
        planetCols = []
    
    #load the hashtable
    hashTable = shelve.open(os.path.join(HYP_DATA_DIR,"hashtable.shelf"))
    resultTable = []
    
    #load the composition table
    if len(elementCols) > 0:
        compositions = pd.read_hdf(os.path.join(HYP_DATA_DIR,"compositions.%s.h5" % int(session.solarnorm)),"compositions")
        #filter based on catalogs
        if session.catalogs and session.cat_action == "exclude":
            compositions = compositions[~compositions.catalogue.isin(session.catalogs)]
        elif session.catalogs:
            compositions = compositions[compositions.catalogue.isin(session.catalogs)]
        #get all the elements we need
        elements = {}
        for item in elementCols:
            if not item.endswith("_err"):
                elements[item] = db(db.t_element.f_name==item+"H").select().first().id
        xy_data = compositions[compositions.element.isin(elements.values())]
        myStars = Stars()
        for abundance in xy_data.itertuples():
            myStars.addAbundance(abundance.star_hip,abundance.element,abundance.value)
    
    #build the table
    for hip in hipData:
        star = hashTable["star-%s" % hip]
        starid = hashTable['starid-%s' % hip]
        planets = []
        for char in "bcdefghijklmnopqrstuvwxyz":
            newPlanet = hashTable.get("planet-%s-%s" % (starid[0],char))
            if not newPlanet:
                break
            planets.append(newPlanet)
        row = []
        for col in starCols1:
            row.append(star.get(col))
        if len(elementCols) > 0:
            for item in elementCols:
                if 'spread' in session.tablecols:
                    if not item.endswith("_err"):
                        row += (myStars.getStatistic(hip,elements[item],rep_error=COL_REP_SPREAD[item],wrapper=True) or ['',''])
                else:
                    row.append(myStars.getStatistic(hip,elements[item],wrapper=True) or '')
        for col in starCols2:
            row.append(star.get(col))
        if planets:
            for col in planetCols:
                row.append([str(planet.get(col)) for planet in planets])
        else:
            for col in planetCols:
                row.append("")
        resultTable.append(row)
    
    #build the columns
    columns = starCols1+elementCols+starCols2+planetCols
    
    #sort the table if necessary
    if request.vars.sort:
        session.reverse = (session.orderby == request.vars.sort and session.reverse != True)
        session.orderby = request.vars.sort
        sort_col = columns.index(request.vars.sort)
        if session.orderby == "f_spec":
            resultTable = sorted(resultTable, key=lambda row: sortable(spectype(row[sort_col]),session.reverse), reverse=session.reverse)
        else:
            resultTable = sorted(resultTable, key=lambda row: sortable(row[sort_col],session.reverse), reverse=session.reverse)
        
    #status text
    status = "%s stars selected" % len(hipData)
    
    #if there are more than 150 stars, show the first 100 and put a link to load more
    if len(hipData) > 150 and request.extension == "load":
        showrows = intable(request.vars.showrows,invalid=100)
        if len(resultTable) > showrows:
            resultTable = resultTable[:intable(request.vars.showrows,invalid=100)]
            moreRows = True
        else:
            moreRows = False
    else:
        moreRows = False
    
    #return the table
    return dict(table=resultTable,status=status,columns=columns,moreRows=moreRows)
    
def hip_details():
    hip = int(request.vars.hip)
    element = int(request.vars.element)
    solarnorm = request.vars.solarnorm
    statistic = request.vars.statistic
    cat_action = request.vars.cat_action
    try:
        catalogs = json.loads(request.vars.catalogs)
    except:
        catalogs = None
    
    compositions = pd.read_hdf(os.path.join(HYP_DATA_DIR,"compositions.%s.h5" % solarnorm),"compositions")
    if catalogs and session.cat_action == "only":
        compositions = compositions[compositions.catalogue.isin(catalogs)]
    
    info = compositions[compositions.star_hip == hip][compositions.element == element]
    
    result = []
    for value in zip(info.value,info.catalogue):
        catalogue = db.t_catalogue[value[1]]
        if catalogue.f_display_name:
            catalogue_name = catalogue.f_display_name
        else:
            catalogue_name = "%s (%s%s)" % (catalogue.f_author,catalogue.f_year,catalogue.f_version)
        result.append((value[0], catalogue_name))
    result = sorted(result,key=lambda row: row[0])
    
    if statistic == "median" and len(result) % 2 == 1:
        middle = [len(result) / 2]
    elif statistic == "median":
        middle = [len(result) / 2 - 1, len(result) / 2]
    else:
        middle = []
        
    return dict(result=result,middle=middle)
    
"""def qc():
    form = SQLFORM.factory(
        Field('query', 'string', default=request.vars.query))
    if form.process().accepted:
        response.flash = 'form accepted'
        hashTable = shelve.open(os.path.join(HYP_DATA_DIR,"hashtable.shelf"))
        result = repr(hashTable.get(form.vars.query)) or "not found"
    elif form.errors:
        response.flash = 'form has errors'
        result = "form has errors"
    else:
        result = "please fill out the form"
    solarnorms = db(db.t_solarnorm.id>0).select()
    catalogs = db(db.t_catalogue.id>0).select()
    elements = db(db.t_element.id>0).select()
    return dict(form=form, result=result, solarnorms=solarnorms, catalogs=catalogs, elements=elements)"""

"""@auth.requires_login()
def catalogue_manage():
    form = SQLFORM.smartgrid(db.t_catalogue,onupdate=auth.archive)
    return locals()

@auth.requires_login()
def composition_manage():
    form = SQLFORM.smartgrid(db.t_composition,onupdate=auth.archive)
    return locals()

@auth.requires_login()
def planet_manage():
    form = SQLFORM.smartgrid(db.t_planet,onupdate=auth.archive)
    return locals()

@auth.requires_login()
def star_manage():
    form = SQLFORM.smartgrid(db.t_star,onupdate=auth.archive)
    return locals()

@auth.requires_login()
def solarnorm_manage():
    form = SQLFORM.smartgrid(db.t_solarnorm,onupdate=auth.archive)
    return locals()

@auth.requires_login()
def element_manage():
    form = SQLFORM.smartgrid(db.t_element,onupdate=auth.archive)
    return locals()
    
@auth.requires_login()
def upload_manage():
    form = SQLFORM.smartgrid(db.t_upload,onupdate=auth.archive)
    return locals()"""

#use web2py scheduler
#db commit at end of the function
@auth.requires_membership("admin")
def import_data():
    def try_to_remove(file):
        try:
            os.remove(file)
        except:pass
    db(db.t_star.id>0).delete()
    db(db.t_planet.id>0).delete()
    db(db.t_element.id>0).delete()
    db(db.t_catalogue.id>0).delete()
    solarnorms = db(db.t_solarnorm.id>0).count()
    for i in range(1,solarnorms+1):
        try_to_remove(os.path.join(HYP_DATA_DIR,"compositions.%s.h5" % i))
        try_to_remove(os.path.join(HYP_DATA_DIR,"compositions-%s.shelf.bak" % i))
        try_to_remove(os.path.join(HYP_DATA_DIR,"compositions-%s.shelf.dat" % i))
        try_to_remove(os.path.join(HYP_DATA_DIR,"compositions-%s.shelf.dir" % i))
        try_to_remove(os.path.join(HYP_DATA_DIR,"compositions-%s.shelf" % i))
    try_to_remove(os.path.join(HYP_DATA_DIR,"compositions.h5"))
    try_to_remove(os.path.join(HYP_DATA_DIR,"hashtable.shelf.bak"))
    try_to_remove(os.path.join(HYP_DATA_DIR,"hashtable.shelf.dat"))
    try_to_remove(os.path.join(HYP_DATA_DIR,"hashtable.shelf.dir"))
    try_to_remove(os.path.join(HYP_DATA_DIR,"hashtable.shelf"))
    elements = db(db.t_element.id>0).select()
    sched_db(sched_db.scheduler_task.task_name=="importDataTask").delete()
    importDataTask()
    sched_db(sched_db.scheduler_task.task_name=="cacheDataTask").delete()
    cacheDataTask()
    redirect(URL("processing"))
    
@auth.requires_membership('admin')
def upload():
    if len(request.vars) == 0:
        redirect(URL('upload',args='t_upload',vars={'keywords':'t_upload.f_uploaded="False"'}))
    form = SQLFORM.smartgrid(db.t_upload,onupdate=auth.archive)
    status = sched_db(sched_db.scheduler_task.id>0).select()
    return locals()
    
@auth.requires_membership('admin')
def processing():
    status = sched_db(sched_db.scheduler_task.id>0).select()
    stars = db(db.t_star.id>0).count()
    return locals()
        
    
### utilities

@auth.requires_membership('owner')
def userlist():
    users = db(db.auth_user.id>0).select()
    return dict(users=users)

@auth.requires_membership('owner')
def makeadmin():
    auth.add_membership('admin',request.args[0])
    redirect(URL("userlist"))

@auth.requires_membership('owner')
def removeadmin():
    auth.del_membership('admin',request.args[0])
    redirect(URL("userlist"))
    
@auth.requires_membership('owner')
def makeowner():
    auth.add_membership('owner',request.args[0])
    redirect(URL("userlist"))

@auth.requires_membership('owner')
def removeowner():
    auth.del_membership('owner',request.args[0])
    redirect(URL("userlist"))
    
def forgot():
    form = SQLFORM.factory(
        Field("user","string",required=True,label="Email"))
    if form.process().accepted:
        user = db(db.auth_user.email==request.vars.user).select().first()
        if user:
            passwordkey = ''.join(random.choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") for x in range(25))
            db(db.auth_user.id==user.id).update(reset_password_key=passwordkey)
            email = user.email
            name = user.first_name
            content = "Use this link to reset your password:"
            link = "http://%s/%s/default/forgot2/%s" % (request.env.http_host,request.application,passwordkey)
            subject = "Password recovery"
            button = "Reset password"
            response.flash = email_user(content,email,subject=subject,button=button,link=link,safety=False)
        else:
            response.flash = "Could not find user."
    elif form.errors:
        response.flash = 'Form has errors.'
    return dict(form=form)
    
def forgot2():
    user = db(db.auth_user.reset_password_key == request.args[0]).select().first()
    form = SQLFORM.factory(
        Field("password","password",required=True,label="Enter new password"),
        Field("password_two","password",required=True,label="Enter new password again"))
    if form.process().accepted:
        if request.vars.password and request.vars.password == request.vars.password_two:
            db(db.auth_user.id==user.id).validate_and_update(reset_password_key="",password=request.vars.password)
            redirect(URL("index"))
        elif not request.vars.password:
            response.flash = "Please enter a password."
        else:
            response.flash = "Passwords do not match."
    elif form.errors:
        response.flash = 'Form has errors.'
    return dict(form=form)
    
def feedback():
    form = SQLFORM(db.feedback)
    if auth.user_id:
        form.vars.name = auth.user.first_name + " " + auth.user.last_name
        form.vars.email = auth.user.email
    if form.process().accepted:
        email_subject = "%s sends feedback" % (form.vars.name or form.vars.email)
        email_content = '%s (%s) submitted the following feedback: %s' % (form.vars.name,form.vars.email,form.vars.zmessage)
        email_link = None
        email_button = None
        email_user(content=email_content,subject=email_subject,link=email_link,button=email_button,email="dan.burger@vanderbilt.edu",safety=False)
        email_subject = "Thank you for your feedback"
        email_content = "Thank you for your recently submitted feedback to the AAVSO Target Tool. Your message will be sent to members of our support team who should be able to respond to your feedback shortly. The AAVSO Target Tool is still in development so we are prioritizing our responses to questions on a selective basis, but generally within 7 days."
        email_user(content=email_content,subject=email_subject,email=form.vars.email, safety=False)
        response.flash = "We have received your feedback. Thank you for your response."
    elif form.errors:
        response.flash = 'form has errors'
    elif auth.user_id:
        form.vars.name = auth.user.first_name + " " + auth.user.last_name
        form.vars.email = auth.user.email
    return dict(form=form)