"""from gluon.contrib.populate import populate
if db(db.t_solarnorm).isempty():
     populate(db.auth_user,10)
     populate(db.t_solarnorm,10)
     populate(db.t_star,10)
     populate(db.t_element,10)
     populate(db.t_planet,10)
     populate(db.t_catalogue,10)
     populate(db.t_composition,10)
"""