from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'Hypatia Catalog Database Website'
settings.subtitle = 'Project Superstar'
settings.author = 'Vanderbilt University'
settings.author_email = 'dan.burger@vanderbilt.edu'
settings.keywords = 'stellar abundance'
settings.description = 'The Hypatia Catalog Database features an interactive table and multiple plotting interfaces that allow easy access and exploration of data within the Hypatia Catalog, a multidimensional, amalgamate dataset comprised of stellar abundance measurements for FGK-type stars within 150 pc of the Sun from carefully culled literature sources that measured both [Fe/H] and at least one other element.'
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = 'db2c6c84-301e-4205-b07c-2940d6e798ae'
settings.email_server = 'localhost'
settings.email_sender = 'you@example.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []
