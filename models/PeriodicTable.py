periodicTable = [
    ["H" ,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,"He"],
    ["Li","Be",None,None,None,None,None,None,None,None,None,None,"B" ,"C" ,"N" ,"O" ,"F" ,"Ne"],
    ["Na","Mg",None,None,None,None,None,None,None,None,None,None,"Al","Si","P" ,"S" ,"Cl","Ar"],
    ["K" ,"Ca","Sc","Ti","V" ,"Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr"],
    ["Rb","Sr","Y" ,"Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I" ,"Xe"],
    ["Cs","Ba","Lu","Hf","Ta","W" ,"Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn"],
    ["Fr","Ra","Lr","Rf","Db","Sg","Bh","Hs","Mt","Ds","Rg","Cn","Nh","Fl","Mc","Lv","Ts","Og"],
    [None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None],
    [None,None,"La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb",None,None],
    [None,None,"Ac","Th","Pa","U" ,"Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No",None,None],
]

def buildPeriodicTable(table_id="pt",showSpecies=False,allowH=False):
    getElements = db(db.t_element.id>0).select()
    supportedElements = [element.f_name for element in getElements]
    if allowH:
        supportedElements.append("HH")
    result = "<table style='margin:0px 10px'>"
    for period in periodicTable:
        result += "<tr style='height:10px'>"
        for element in period:
            result += "<td>"
            if element is not None:
                has_i = element+"H" in supportedElements
                has_ii = element+"IIH" in supportedElements
                if has_i and has_ii and showSpecies:
                    result += "<nobr><btn title='%(desc)s' class='btn btn-default btn-xs btn-%(table_id)s' id='%(table_id)s-%(element)s' onclick='pick(\"%(table_id)s\",\"%(element)s\")'>%(element)s</btn><btn title='%(desc)s (II)' class='btn btn-default btn-xs btn-%(table_id)s' id='%(table_id)s-%(element)sII' onclick='pick(\"%(table_id)s\",\"%(element)sII\")'>II</btn><nobr>" % dict(table_id=table_id,element=element,desc=COL_LONG_DESC[element])
                elif has_i or (has_ii and not showSpecies):
                    result += "<btn title='%(desc)s' class='btn btn-default btn-xs btn-%(table_id)s' id='%(table_id)s-%(element)s' onclick='pick(\"%(table_id)s\",\"%(element)s\")'>%(element)s</btn>" % dict(table_id=table_id,element=element,desc=COL_LONG_DESC[element])
                elif has_ii and showSpecies:
                    result += "<nobr>%(element)s<btn title='%(desc)s (II)' class='btn btn-default btn-xs btn-%(table_id)s' id='%(table_id)s-%(element)sII' onclick='pick(\"%(table_id)s\",\"%(element)sII\")'>II</btn></nobr>" % dict(table_id=table_id,element=element,desc=COL_LONG_DESC[element])
                else:
                    result += element
            result += "</td>"
        result += "</tr>"
    result += "</table>"
    return result