class Stars:
    stars = {}
    
    def addAbundance(self,hip,element,val):
        if self.stars.get(hip) == None:
            self.stars[hip] = {}
        if self.stars[hip].get(element) == None:
            self.stars[hip][element] = []
        self.stars[hip][element].append(val)
        
    def getStatistic(self,hip,element,rep_error=False,wrapper=False):
        if session.statistic == "mean":
            result = self.getMean(hip,element,rep_error)
        else:
            result = self.getMedian(hip,element,rep_error)
        if wrapper and rep_error and result and not request.vars.download:
            return [XML('<a style="cursor:pointer" onclick="popover(this,\'/hypatia/default/hip_details?hip=%s&element=%s&solarnorm=%s&statistic=%s&cat_action=%s&catalogs=%s\')">%.2f</a>' % (hip,element,session.solarnorm,session.statistic,session.cat_action,session.catalogs,result[0])),result[1]]
        elif wrapper and result and not request.vars.download:
            return XML('<a style="cursor:pointer" onclick="popover(this,\'/hypatia/default/hip_details?hip=%s&element=%s&solarnorm=%s&statistic=%s&cat_action=%s&catalogs=%s\')">%.2f</a>' % (hip,element,session.solarnorm,session.statistic,session.cat_action,session.catalogs,result))
        else:
            return result
    
    def getMedian(self,hip,element,rep_error=False):
        if self.stars.get(hip) == None:
            return None
        if self.stars[hip].get(element) == None:
            return None
        values = self.stars[hip][element]
        if rep_error and len(values) > 1:
            return [median(values),(max(values)-min(values))/2.0]
        elif rep_error:
            return [median(values),rep_error]
        else:
            return median(values)
    
    def getMean(self,hip,element,rep_error=False):
        if self.stars.get(hip) == None:
            return None
        if self.stars[hip].get(element) == None:
            return None
        values = self.stars[hip][element]
        if rep_error and len(values) > 1:
            return [mean(values),(max(values)-min(values))/2.0]
        elif rep_error:
            return [mean(values),rep_error]
        else:
            return mean(values)
    
    def getHips(self):
        return self.stars.keys()
    
    def printData(self):
        print repr(self.stars)
        
# code from Nitin
def median(arr):
    """
    Computes the median for a list of numbers
    :param arr: list of integers
    :return: median
    """
    arr.sort()
    length = len(arr)
    if length % 2 == 0:
        return (arr[length / 2] + arr[(length / 2) - 1]) / 2
    return arr[length / 2]


def mean(arr):
    """
    Computes the mean of an array of numbers
    :param arr: list of numbers
    :return: mean
    """
    return sum(arr) / len(arr)