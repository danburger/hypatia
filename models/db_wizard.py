### we prepend t_ to tablenames and f_ to fieldnames for disambiguity


########################################
db.define_table('t_solarnorm',
    Field('f_author', type='string',
          label=T('Author')),
    Field('f_year', type='integer',
          label=T('Year')),
    Field('f_version', type='string',
          label=T('Version')),
    Field('f_notes', type='string',
          label=T('Notes')),
    Field('f_identifier', type="string",
          label=T('Identifier')),
    auth.signature,
    format='%(f_author)s (%(f_year)s%(f_version)s) %(f_notes)s',
    migrate=settings.migrate)

db.define_table('t_solarnorm_archive',db.t_solarnorm,Field('current_record','reference t_solarnorm',readable=False,writable=False))

########################################
db.define_table('t_star',
    Field('f_hip', type='integer',
          label=T('Hip')),
    Field('f_hd', type='integer',
          label=T('Hd')),
    Field('f_bd', type='string',
          label=T('Bd')),
    Field('f_spec', type='string',
          label=T('Spec')),
    Field('f_vmag', type='double',
          label=T('Vmag')),
    Field('f_bv', type='double',
          label=T('Bv')),
    Field('f_dist', type='double',
          label=T('Dist')),
    Field('f_ra', type='double',
          label=T('Ra')),
    Field('f_dec', type='double',
          label=T('Dec')),
    Field('f_x', type='double',
          label=T('X')),
    Field('f_y', type='double',
          label=T('Y')),
    Field('f_z', type='double',
          label=T('Z')),
    Field('f_disk', type='string',
          label=T('Disk')),
    Field('f_u', type='double',
          label=T('U')),
    Field('f_v', type='double',
          label=T('V')),
    Field('f_w', type='double',
          label=T('W')),
    Field('f_teff', type='double',
          label=T('Teff')),
    Field('f_logg', type='double',
          label=T('Logg')),
    Field('f_mass', type='double',
          label=T('Mass')),
    Field('f_radius', type='double',
          label=T('Radius')),
    Field('f_number_of_planets', type='integer',
          label=T('Number of Planets')),
    Field('f_2mass', type='string',
          label=T('2MASS')),
    Field('f_ra_proper_motion', type='double',
          label=T('Ra Proper Motion')),
    Field('f_dec_proper_motion', type='double',
          label=T('Dec Proper Motion')),
    Field('f_bmag', type='double',
          label=T('Bmag')),
    auth.signature,
    format='%(f_hip)s',
    migrate=settings.migrate)

db.define_table('t_star_archive',db.t_star,Field('current_record','reference t_star',readable=False,writable=False))

########################################
db.define_table('t_element',
    Field('f_name', type='string',
          label=T('Name')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)

db.define_table('t_element_archive',db.t_element,Field('current_record','reference t_element',readable=False,writable=False))

########################################
db.define_table('t_planet',
    Field('f_name', type='string',
          label=T('Name')),
    Field('f_star', type='reference t_star',
          label=T('Star')),
    Field('f_m_p', type='double',
          label=T('M P')),
    Field('f_m_p_err', type='double',
          label=T('M P Err')),
    Field('f_p', type='double',
          label=T('P')),
    Field('f_p_err', type='double',
          label=T('P Err')),
    Field('f_e', type='double',
          label=T('E')),
    Field('f_e_err', type='double',
          label=T('E Err')),
    Field('f_a', type='double',
          label=T('A')),
    Field('f_a_err', type='double',
          label=T('A Err')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)

db.define_table('t_planet_archive',db.t_planet,Field('current_record','reference t_planet',readable=False,writable=False))

########################################
db.define_table('t_catalogue',
    Field('f_author', type='string',
          label=T('Author')),
    Field('f_year', type='integer',
          label=T('Year')),
    Field('f_version', type='string',
          label=T('Version')),
    Field('f_li', type='string',
          label=T('Li')),
    Field('f_display_name', type='string',
          label=T('Display Name')),
    Field('f_identifier', type="string",
          label=T('Identifier')),
    auth.signature,
    format='%(f_author)s (%(f_year)s%(f_version)s)',
    migrate=settings.migrate)

db.define_table('t_catalogue_archive',db.t_catalogue,Field('current_record','reference t_catalogue',readable=False,writable=False))

########################################
db.define_table('t_composition',
    Field('f_solarnorm', type='reference t_solarnorm',
          label=T('Solarnorm')),
    Field('f_star', type='reference t_star',
          label=T('Star')),
    Field('f_catalogue', type='reference t_catalogue',
          label=T('Catalogue')),
    Field('f_element', type='reference t_element',
          label=T('Element')),
    Field('f_nlte', type='boolean',
          label=T('NLTE')),
    Field('f_value', type='double',
          label=T('Value')),
    auth.signature,
    format='%(f_solarnorm)s',
    migrate=settings.migrate)
    
db.define_table('t_composition_archive',db.t_catalogue,Field('current_record','reference t_composition',readable=False,writable=False))

    
########################################
db.define_table('t_upload',
    Field('f_file', type='upload',
          label=T('File')),
    Field('f_solarnorm', type='reference t_solarnorm',
          label=T('Solarnorm')),
    Field('f_uploaded', type='boolean', default=False,
          label=T('Uploaded'), writable=False),
    auth.signature,
    format='%(id)s',
    migrate=settings.migrate)

db.define_table('t_upload_archive',db.t_composition,Field('current_record','reference t_upload',readable=False,writable=False))

db.define_table('t_feedback',
    Field("f_name","string",required=True,label=T('Name')),
    Field("f_email","string",required=True,label=T('Email')),
    Field("f_message","text",label=T("Message")),
    auth.signature,
    migrate=settings.migrate)
    
db.define_table('t_announcement',
    Field("f_message","string",required=True,label=T("Message")),
    auth.signature,
    migrate=settings.migrate)