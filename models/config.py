import os, ConfigParser
config = ConfigParser.RawConfigParser()
config.read(os.path.join(request.env.gluon_parent,"hypatia.cfg"))

try:
    HYP_MAILJET_APIKEY = config.get("mailjet","apikey")
except:
    HYP_MAILJET_APIKEY = None
    
try:
    HYP_MAILJET_SECRETKEY = config.get("mailjet","secretkey")
except:
    HYP_MAILJET_SECRETKEY = None
    
#try:
HYP_MAILJET_FROM = config.get("mailjet","from")
#except:
#    HYP_MAILJET_FROM = None

HYP_DATA_DIR = config.get("data","directory")