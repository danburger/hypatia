import os
import copy
import pandas as pd
import shelve
import roman
try:
    from mailjet_rest import Client
    HYP_HAS_MAILJET = True
except:
    HYP_HAS_MAILJET = False

def intable(x, invalid=None):
    try:
        return int(x)
    except:
        return invalid

def floatable(x, invalid=None):
    try:
        return float(x)
    except:
        return invalid
        
def sortable(x,reverse):
    if x in [None,"999.0",9999,""]:
        if reverse:
            return -1e20
        else:
            return 1e20
    else:
        return x
        
#ref: https://arxiv.org/ftp/arxiv/papers/1108/1108.4971.pdf
def spectype(s):
    if s is None:
        return None
    typechar = []
    digits = []
    romanNumerals = []
    currDigit = ""
    currRomanNumeral = ""
    for char in s+" ":
        if char == "F":
            typechar.append(10)
        elif char == "G":
            typechar.append(20)
        elif char == "K":
            typechar.append(30)
        elif char == "M":
            typechar.append(40)
            
        if char in ["0","1","2","3","4","5","6","7","8","9","."]:
            currDigit += char
        elif len(currDigit) > 0:
            try:
                digits.append(float(currDigit))
            except:pass
            currDigit = ""
            
        if char in ["I","V","X"]:
            currRomanNumeral += char
        elif len(currRomanNumeral) > 0:
            try:
                romanNumerals.append(roman.fromRoman(currRomanNumeral))
            except:pass
            currRomanNumeral = ""
            
    firstType = 0
    if len(typechar) > 0:
        firstType = typechar[0]
    firstDigit = 0
    if len(digits) > 0:
        firstDigit = digits[0]
    firstRomanNumeral = 0
    if len(romanNumerals) > 0:
        firstRomanNumeral = float(romanNumerals[0])
    return firstType + firstDigit + (firstRomanNumeral/100)
        
def check_filter(x,filter):
    if filter[2]:
        return not check_filter(x,(filter[0],filter[1],False))
    if filter[0] != None and filter[1] != None:
        return (filter[0] <= x) and (x <= filter[1])
    elif filter[0] != None:
        return (filter[0] <= x)
    elif filter[1] != None:
        return (x <= filter[1])
    else:
        return True

def cacheDataTask(queued=False):
    if not queued:
        scheduler.queue_task(cacheDataTask,
                             immediate=True,
                             timeout=3600,
                             pvars=dict(queued=True),
                             sync_output=2)
        return "Added to scheduler"
    solarnorms = db(db.t_solarnorm.id>0).select()
    dataframes = []
    for s in solarnorms:
        compositions_tbl = []
        print "Selecting data..."
        compositions = shelve.open(os.path.join(HYP_DATA_DIR,"compositions-%s.shelf" % s.id))
        print "Building table..."
        for row in compositions.iterkeys():
            rowspl = [int(item) for item in row.split("-")] + [compositions[row]]
            compositions_tbl.append(rowspl)
        print "Building dataframe..."
        cache = pd.DataFrame(compositions_tbl,columns=["solarnorm","star_id","star_hip","catalogue","element","nlte","value"])
        print "Saving..."
        dataframes.append(cache)
        cache.to_hdf(os.path.join(HYP_DATA_DIR,"compositions.%s.h5" % s.id),'compositions')
    #print "Merging together..."
    #pd.concat(dataframes).to_hdf(os.path.join(HYP_DATA_DIR,"compositions.h5"),'compositions')
    print "Done!"
    return "Done"

def importDataTask(queued=False,filePath=None,solarnorm=None):
    if not queued:
        uploads = db(db.t_upload.f_uploaded==False).select()
        for u in uploads:
            print os.path.join(request.folder,"uploads",u.f_file)
            db(db.t_upload.id==u.id).update(f_uploaded=True)
            scheduler.queue_task(importDataTask,
                                 immediate=True,
                                 timeout=3600,
                                 pvars=dict(queued=True,
                                            filePath=os.path.join(request.folder,"uploads",u.f_file),
                                            solarnorm=u.f_solarnorm),
                                 sync_output=2)
        return "Added to scheduler"
    hashtable = shelve.open(os.path.join(HYP_DATA_DIR,"hashtable.shelf"))
    compositions = shelve.open(os.path.join(HYP_DATA_DIR,"compositions-%s.shelf"%solarnorm))
    with open(filePath) as infile:
        numberOfChars = len(infile.read())
    with open(filePath) as infile:
        blankPlanet = dict.fromkeys([
                                'f_name',
                                'f_m_p',
                                'f_m_p_err',
                                'f_p',
                                'f_p_err',
                                'f_e',
                                'f_e_err',
                                'f_a',
                                'f_a_err'])
        blankNewData = dict.fromkeys([
                                'f_hip',
                                'f_hd',
                                'f_bd',
                                'f_spec',
                                'f_vmag',
                                'f_bv',
                                'f_dist',
                                'f_ra',
                                'f_dec',
                                'f_x',
                                'f_y',
                                'f_z',
                                'f_disk',
                                'f_u',
                                'f_v',
                                'f_w',
                                'f_teff',
                                'f_logg',
                                'f_mass',
                                'f_radius',
                                'f_number_of_planets',
                                'f_2mass',
                                'f_ra_proper_motion',
                                'f_dec_proper_motion',
                                'f_bmag'])
        newData = copy.deepcopy(blankNewData)
        newAuthor = []
        newYear = []
        newElement = []
        #added
        newElementValue = []
        newNLTE = []
        newVersion = []
        newPlanetData = {}
        newLi = []
        
        starNo = 1
        curPos = 0
        
        for line in infile:
            curLine = line.split()
            curPos += len(line)
            #print len(curLine)
            
            if len(curLine) > 2:
                if curLine[0] == 'Star:':
                    val = int(curLine[3])
                    newData['f_hip'] = val
                if curLine[0] == 'hd':
                    val = curLine[2]
                    val = val.replace("[", "")
                    val = val.replace("]", "")
                    val = val.replace("'", "")
                    val = int(val)
                    newData['f_hd'] = val
                if curLine[0] == 'bd':
                    val = curLine[2] + ' ' + curLine[3]
                    newData['f_bd'] = val
                if curLine[0] == '2MASS':
                    val = curLine[2]
                    newData['f_2mass'] = val
                if curLine[0] == 'dist':
                    val = float(curLine[3])
                    newData['f_dist'] = val
                if curLine[0] == 'Spec':
                    val = curLine[3]
                    newData['f_spec'] = val
                if curLine[0] == 'Vmag':
                    val = float(curLine[2])
                    newData['f_vmag'] = val
                if curLine[0] == 'Bmag':
                    val = float(curLine[2])
                    newData['f_bmag'] = val
                if curLine[0] == 'B-V':
                    val = curLine[2]
                    if(val != 'nan'):
                        newData['f_bv'] = float(val)
                    else:
                        newData['f_bv'] = None
                if curLine[0] == 'RA' and curLine[1] != 'proper':
                    val = float(curLine[2])
                    newData['f_ra'] = val
                if curLine[0] == 'RA' and curLine[1] == 'proper':
                    val = float(curLine[4])
                    newData['f_ra_proper_motion'] = val
                if curLine[0] == 'Dec' and curLine[1] != 'proper':
                    val = float(curLine[2])
                    newData['f_dec'] = val
                if curLine[0] == 'Dec' and curLine[1] == 'proper':
                    val = float(curLine[4])
                    newData['f_dec_proper_motion'] = val
                if curLine[0] == 'Position':
                    x = curLine[2]
                    y = curLine[3]
                    z = curLine[4]
                    x = float(x[1:len(x)-1])
                    y = float(y[0:len(y)-1])
                    z = float(z[0:len(z)-1])
                    newData['f_x'] = x
                    newData['f_y'] = y
                    newData['f_z'] = z
                if curLine[0] == 'Disk':
                    val = curLine[2]
                    newData['f_disk'] = val
                if curLine[0] == 'UVW':
                    u = curLine[2]
                    v = curLine[3]
                    w = curLine[4]
                    u = float(u[1:len(u)-1])
                    v = float(v[0:len(v)-1])
                    w = float(w[0:len(w)-1])
                    newData['f_u'] = u
                    newData['f_v'] = v
                    newData['f_w'] = w
                if curLine[0] == 'Teff':
                    val = curLine[2]
                    if(val != 'nan'):
                        newData['f_teff'] = float(val)
                    else:
                        newData['f_teff'] = None
                if curLine[0] == 'logg':
                    val = curLine[2]
                    if(val != 'nan'):
                        newData['f_logg'] = float(val)
                    else:
                        newData['f_logg'] = None
                if curLine[0] == 'mass(M_S)':
                    val = float(curLine[2])
                    newData['f_mass'] = val
                if curLine[0] == 'radius':
                    val = float(curLine[3])
                    newData['f_radius'] = val
                if curLine[0] == 'Number':
                    val = int(curLine[4])
                    newData['f_number_of_planets'] = val
                #Added planets stuffs
                if curLine[0][0] == '[':
                    planet = copy.deepcopy(blankPlanet)
                    
                    name = curLine[0]
                    name = name.replace("[", "")
                    name = name.replace("]", "")
                    planet['f_name'] = name
                    
                    planet['f_m_p'] = float(curLine[3])
                    mp_err = curLine[5]
                    mp_err = mp_err.replace("(M_J),", "")
                    planet['f_m_p_err'] = float(mp_err)
                    
                    planet['f_p'] = float(curLine[8])
                    p_err = curLine[10]
                    p_err = p_err.replace("(d),", "")
                    planet['f_p_err'] = float(p_err)
                                
                    planet['f_e'] = float(curLine[13])
                    e_err = curLine[15]
                    e_err = e_err.replace(",", "")
                    planet['f_e_err'] = float(e_err)  
                    
                    planet['f_a'] = float(curLine[18])
                    a_err = curLine[20]
                    a_err = a_err.replace("(AU)", "")
                    planet['f_a_err'] = float(a_err)
                    
                    newPlanetData[name] = planet
                
                if curLine[0][-1] == 'H': #checking for elements data
                    version = ''
                    element = curLine[0]
                    
                    if(curLine[1] == '(NLTE)'):
                        curLine.pop(1)
                        newNLTE.append(1)
                    else:
                        newNLTE.append(0)
                    
                    elementValue = float(curLine[1])
                    author = curLine[2][1:len(curLine[2])] #gets rid of [
                    for i in range(3, len(curLine)):
                        if curLine[i][0] != '(':
                            author = author + ' ' + curLine[i]
                    
                    year = curLine[len(curLine) - 1] #isolates year
                    year = year.replace("(", "")
                    year = year.replace(")", "")
                    year = year.replace("]", "")
                    
                    tmpYr = year
                    
                    year = year.replace("-Li","")
                    
                    if tmpYr != year:
                        li = True
                    else:
                        li = False
                        
                    tmpYr = year
                    
                    for letter in "abcdefghijklmnopqrstuvwxyz":
                        year = year.replace(letter, "")
                        if tmpYr != year:
                            version = letter
                        
                    year = year.replace(",", "")
                    
                    newElement.append(element)
                    newAuthor.append(author)
                    newYear.append(year)
                    newVersion.append(version)
                    newLi.append(li)
                    newElementValue.append(elementValue)
            if len(curLine) == 0 or curPos == numberOfChars:
                starId = []
                hipNum = newData['f_hip']
                if(hipNum != None):
                    print("Adding star #%s: %s" % (starNo,hipNum))
                    starNo = starNo + 1
                    
                    if hashtable.get("star-%s" % hipNum) != newData:
                        db.t_star.update_or_insert(db.t_star.f_hip == hipNum,**newData)
                        hashtable['star-%s' % hipNum] = newData
                   
                    if hashtable.get("starid-%s" % hipNum):
                        starId = hashtable['starid-%s' % hipNum]
                    else:
                        star = db(db.t_star.f_hip == newData['f_hip']).select()
                        for row in star:
                            starId.append(row.id)
                        hashtable['starid-%s' % hipNum] = starId
                
                for i in range(0, len(newNLTE)):
                    element_ = newElement[i]
                    nlte_ = newNLTE[i]
                    elementValue_ = newElementValue[i]
                    author_ = newAuthor[i]
                    version_ = newVersion[i]
                    li_ = newLi[i]
                    year_ =  str(newYear[i])
                    version = newVersion[i]

                    if not hashtable.has_key("element-%s" % element_):
                        db.t_element.update_or_insert(db.t_element.f_name == element_,f_name = element_)
                        hashtable['element-%s' % element_] = 1
                    
                    if hashtable.get("elementid-%s" % element_):
                        elemId = hashtable['elementid-%s' % element_]
                    else:
                        elem = db(db.t_element.f_name == element_).select()
                        elemId = []
                        for row in elem:
                            elemId.append(row.id)
                        hashtable['elementid-%s' % element_] = elemId
                    
                    if not hashtable.has_key("catalogue-%s-%s-%s-%s" % (author_,year_,version_,li_)):
                        db.t_catalogue.update_or_insert(
                            (db.t_catalogue.f_author == author_)&
                            (db.t_catalogue.f_year == year_)&
                            (db.t_catalogue.f_version == version_)&
                            (db.t_catalogue.f_li == li_), 
                            f_author=author_, 
                            f_year=year_, 
                            f_version = version_,
                            f_li = li_)
                        hashtable["catalogue-%s-%s-%s-%s" % (author_,year_,version_,li_)] = 1
                        
                    if hashtable.has_key("catalogueid-%s-%s-%s-%s" % (author_,year_,version_,li_)):
                        catId = hashtable["catalogueid-%s-%s-%s-%s" % (author_,year_,version_,li_)]
                    else:
                        cat = db((db.t_catalogue.f_author == author_)&(db.t_catalogue.f_year == year_)&(db.t_catalogue.f_version == version_)).select()
                        catId = []
                        for row in cat:
                            catId.append(row.id)
                        hashtable["catalogueid-%s-%s-%s-%s" % (author_,year_,version_,li_)] = catId
                        
                    compositions['%s-%s-%s-%s-%s-%s' % (solarnorm,starId[0],hipNum,catId[0],elemId[0],nlte_)] = elementValue_
                    

                for key in newPlanetData:
                    name = None
                    mp = None
                    mperr = None
                    p = None
                    perr = None
                    e = None
                    eerr = None
                    a = None
                    aerr = None
                    for k in newPlanetData[key]:
                        if k == 'f_name':
                            name = newPlanetData[key][k]
                        if k == 'f_m_p':
                            mp = newPlanetData[key][k]
                        if k == 'f_m_p_err':
                            mperr = newPlanetData[key][k]
                        if k == 'f_p':
                            p = newPlanetData[key][k]
                        if k == 'f_p_err':
                            perr = newPlanetData[key][k]
                        if k == 'f_e':
                            e = newPlanetData[key][k]
                        if k == 'f_e_err':
                            eerr = newPlanetData[key][k]
                        if k == 'f_a':
                            a = newPlanetData[key][k]
                        if k == 'f_a_err':
                            aerr = newPlanetData[key][k]
                    if hashtable.get('planet-%s-%s' % (starId[0],name)) != newPlanetData[key]:
                        db.t_planet.update_or_insert(
                            (db.t_planet.f_star == starId[0])&
                            (db.t_planet.f_name == name)&
                            (db.t_planet.f_m_p == mp)&
                            (db.t_planet.f_m_p_err == mperr)&
                            (db.t_planet.f_p == p)&
                            (db.t_planet.f_p_err == perr)&
                            (db.t_planet.f_e == e)&
                            (db.t_planet.f_e_err == eerr)&
                            (db.t_planet.f_a == a)&
                            (db.t_planet.f_a_err == aerr),
                            f_star = starId[0],
                            **(newPlanetData[key]))
                        hashtable['planet-%s-%s' % (starId[0],name)] = newPlanetData[key]
                   
                newData = copy.deepcopy(blankNewData)
                newAuthor = []
                newYear = []
                newElement = []
                newNLTE = []
                newVersion = []
                newLi = []
                newSolarnorm = []
                newElementValue = []
                newPlanetData = {}
    db(db.t_star.f_hip==None).delete() 
    hashtable.close()
    compositions.close()
    db.commit()
    return "Done"
    
def email_user(content,email,subject="A message from Hypatia Catalog",link=None,button=None,safety=True):
    print email
    print subject
    print response.render("email.txt",dict(content=content,link=link,subject=subject,button=button))
    if safety:
        pass
    elif HYP_HAS_MAILJET and HYP_MAILJET_APIKEY:
        mailjet = Client(auth=(HYP_MAILJET_APIKEY, HYP_MAILJET_SECRETKEY))
        mj_email = {
            'FromName': 'Hypatia Catalog',
            'FromEmail': HYP_MAILJET_FROM,
            'Subject': subject,
            'Html-Part': response.render("email.html",dict(content=content,link=link,subject=subject,button=button)),
            'Text-Part': response.render("email.txt" ,dict(content=content,link=link,subject=subject,button=button)),
            'Recipients': [{'Email': email}]
        }
        mailjet.send.create(mj_email)
    else:
        htmlpart = response.render("email.html",dict(content=content,link=link,subject=subject,button=button)),
        textpart = response.render("email.txt" ,dict(content=content,link=link,subject=subject,button=button)),
        mail.send(email,subject,(textpart,htmlpart))
    return "Message sent."